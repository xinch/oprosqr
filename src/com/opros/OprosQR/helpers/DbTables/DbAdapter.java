package com.opros.OprosQR.helpers.DbTables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Admin on 11.08.2014.
 */
public class DbAdapter implements QuerryTable{

    private Context context;
    private SQLiteDatabase database;
    private dbHelper dbHelper;

    public DbAdapter (Context context){
        this.context = context;
    }

    //region Helpers
    public void openHelpers(){
        dbHelper = new com.opros.OprosQR.helpers.DbTables.dbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void closeHelper(){
        dbHelper.close();
        database.close();
    }
    //endregion

    //region Work with querry table

     public void insertQuerry(String link){
         try{
             database.beginTransaction();

             ContentValues cv = new ContentValues();
             cv.put(QuerryTable.colLink,link);
             database.insert(QuerryTable.querry,QuerryTable.colID,cv);
             database.setTransactionSuccessful();
         }finally {
             database.endTransaction();
         }
     }

    public Cursor getLinks(){
        Cursor cursor;
        try {
            database.beginTransaction();
            cursor=database.rawQuery("SELECT "+QuerryTable.colID+" FROM "+QuerryTable.querry,null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        return cursor;
    }
    //endregion

    //region Global

    public void clearTable(String tableName){
        try {
            database.beginTransaction();
            database.execSQL("DELETE FROM "+tableName);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }
    //endregion

}
