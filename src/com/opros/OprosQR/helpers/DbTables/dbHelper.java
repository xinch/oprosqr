package com.opros.OprosQR.helpers.DbTables;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by Admin on 11.08.2014.
 */
public class dbHelper extends SQLiteOpenHelper implements QuerryTable {

    private static final String DATBASE_NAME="MobileData";
    private static final int DATABASE_VERSION=1;

    private static final String DROP_TABLE_QUERRY="DROP TABLE IF EXISTS ";

    public dbHelper(Context context) {
        super(context, DATBASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+QuerryTable.querry+"( "+QuerryTable.colID+" INTEGER NOT NULL PRIMARY KRY AUTOINCREMENT, "+QuerryTable.colLink+" STRING NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
             db.execSQL(DROP_TABLE_QUERRY+QuerryTable.querry);

        onCreate(db);
    }
}
