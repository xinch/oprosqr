package com.opros.OprosQR.helpers;

/**
 * Created by Alexey on 07.08.2014.
 */
public class UrlWork {

    private String _url;

    public UrlWork(String data) {
        _url = data;
    }


    public boolean isHTTP(){

        return true;
    }

    public String httpAdd(){
        String outStr;
        if (_url.indexOf("http://")==-1){
            outStr = "http://" + _url;
        }
        else{
            outStr=_url;
        }
        return outStr;
    }
}
