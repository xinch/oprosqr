package com.opros.OprosQR;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.opros.OprosQR.helpers.SimpleScannerFragment;
import com.opros.OprosQR.helpers.UrlWork;

/*
* Created by Alexey on 04.08.2014.
 */

public class OprosQrActivity extends FragmentActivity implements com.opros.OprosQR.helpers.ScannerListener {

    private static final long DELAY = 5000;
    private static final Handler handler = new Handler();
    private SimpleScannerFragment simpleScannerFragment;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            final Activity activity = this;
            setContentView(R.layout.main_qr_scanner);

            simpleScannerFragment = (SimpleScannerFragment) getSupportFragmentManager().findFragmentById(R.id.scannerFragment);
            simpleScannerFragment.setScannerListener(this);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    configureFullScreen(activity, true);
                }
            }, DELAY);

        }
    private static void configureFullScreen(Activity activity, boolean isEnabled) {
        if (isEnabled) {
            WindowManager.LayoutParams attrs = activity.getWindow().getAttributes();
            attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            activity.getWindow().setAttributes(attrs);
        } else {
            WindowManager.LayoutParams attrs = activity.getWindow().getAttributes();
            attrs.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
            activity.getWindow().setAttributes(attrs);
        }
    }


        @Override
        public void onDataReceive(String data, int barcodeType) {
            // при наведении на код, получаем данные и тип кода
            //Toast.makeText(this, data, Toast.LENGTH_LONG).show();
            UrlWork urlWork = new UrlWork(data);
            Uri address = Uri.parse(urlWork.httpAdd());
            Intent openlink = new Intent(Intent.ACTION_VIEW, address);
            this.startActivity(openlink);
            simpleScannerFragment.onPause();
        }
}

